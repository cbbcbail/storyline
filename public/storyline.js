// imports
const convolution = require('ml-convolution'); // import the convolution package

// global vars
var story;
var functionToWindow;
var svgWidth;
var svgHeight;
var gaussianHeight;
var sidePadding;
var sentimentRange;
var sentiment;
var markerIndex = 0;
var markerX;

// html elements
const marker = document.getElementById("marker");
const svg = document.getElementById("svg");
const gaussianSlider = document.getElementById("gaussianSlider");
const titles = ["aChristmasCarol", "alicesAdventuresInWonderland",
                "aMidsummerNightsDream", "aTaleOfTwoCities", "dracula",
                "frankenstein", "greatExpectations", "hamlet",
                "heartOfDarkness", "littleWomen", "macbeth", "moby-Dick",
                "prideAndPrejudice", "romeoAndJuliet", "theCountOfMonteCristo",
                "theIliad", "theOdyssey", "thePictureOfDorianGray",
                "theScarletLetter", "theSecretGarden", "treasureIsland",
                "uncleTomsCabin", "warAndPeace"];
var index;

// initialization when page is loaded
window.onload = loadPage();

// --- Event Functions ---------------------------------------------------------

function loadPage() {
    markerX = 0;
    const params = new URLSearchParams(window.location.search);
    index = params.get("index");
    if (index == null) {
        index = Math.floor(Math.random()*titles.length)
    }

    // initialize onclick functions for cardLeft and cardRight
    document.getElementById("cardLeft").onclick = function() {
        if (index == 0) {
            index = titles.length - 1;
        }
        else {
            index--;
        }
        loadStory(index);
    };
    document.getElementById("cardRight").onclick = function() {
        if (index == titles.length - 1) {
            index = 0;
        }
        else {
            index++;
        }
        loadStory(index);
    };

    // initialize gaussian slider
    gaussianSlider.min = 2;
    gaussianSlider.max = 16;
    gaussianSlider.value = 7;
    gaussianSlider.step = 0.1;
    gaussianSlider.onchange = loadSlider;
    gaussianSlider.oninput = loadSlider;

    // add event listeners for resizing ot reorienting window
    window.addEventListener('resize', loadPlot, false);
    window.addEventListener('orientationchange', loadPlot, false);
    svg.addEventListener('mousemove', moveMarker);
    loadStory(index);
    loadPlot();
}

function loadStory(index) {
    // load title from the set
    markerX = 0;
    gaussianSlider.value = 7;
    functionToWindow = gaussianSlider.value/100;
    getJSON(titles[index] + ".json", getJSONCallback);
}

function loadSlider() { // event function for slider change/input
    functionToWindow = gaussianSlider.value/100;
    drawGaussian();
    if (story) {
        drawSentiment();
        moveMarker();
    }
}

function loadPlot() { // event function for window resize/reorientation
    svgSize();
    drawGaussian();
    if (story) {
        drawSentiment();
        moveMarker();
    }
}

function moveMarker() {
    var min = sidePadding;
    var max = svgWidth - sidePadding;
    var pt = svg.createSVGPoint();

    if (event !== undefined && event.clientX !== undefined) {
        pt.x = event.clientX;
        var svgP = pt.matrixTransform(svg.getScreenCTM().inverse());
        markerX = svgP.x;
    }

    if (markerX < min) {
        markerX = min;
    }
    else if (markerX > max) {
        markerX = max;
    }

    markerIndex = nearestIndex(markerX, sentimentRange);
    marker.setAttribute("cx", markerX);
    marker.setAttribute("cy", sentiment[markerIndex]);
    marker.setAttribute("visibility", "visible");
    loadText();
}

function svgSize() { // sets canvas size
    const widthRatio = 0.75; // ratio of canvas width to window width
    const heightRatio = 0.2; // ratio of canvas width to canvas height

    svgWidth = window.innerWidth*widthRatio;
    svgHeight = Math.min(window.innerHeight*heightRatio, svgWidth*heightRatio);
    svg.setAttribute("viewBox", "0 0 " + svgWidth + " " + svgHeight);
}

// --- Drawing Functions -------------------------------------------------------

function drawGaussian() { // draws gaussian function in corner of canvas
    gaussianHeight = svgHeight/15;
    var gaussianWidth = Math.floor(svgWidth/20);
    var bottomPadding = svgHeight/25;
    var rightSidePadding = Math.floor(svgWidth/60);

    var xOrigin = svgWidth - gaussianWidth - rightSidePadding;
    var yOrigin = svgHeight - bottomPadding;

    var gaussianRange = range(xOrigin, xOrigin + gaussianWidth - 1);
    var gaussianArray = gaussian(gaussianWidth, functionToWindow);
    gaussianArray = gaussianArray.map(point => yOrigin - gaussianHeight*point);

    var zipGaussian = zip(gaussianRange, gaussianArray);

    gaussianLine = document.getElementById("gaussianLine");
    gaussianLine.setAttribute("points", zipGaussian);
    gaussianLine.setAttribute("stroke-linejoin", "round");
}

function drawSentiment() { // draws the canvas paths on the canvas context
    var dataSize = story.sentiments.length;
    var windowToOverall = 2/3; // relate gaussian window size to overall dataset
    var windowSize = Math.floor(dataSize*windowToOverall);

    var gaussianArray = gaussian(windowSize, functionToWindow);
    sentiment = convolution.fftConvolution(story.sentiments, gaussianArray);
    sentiment = rescale(sentiment);

    var bottomPadding = svgHeight/5;
    var topPadding = svgHeight/25;
    sidePadding = Math.floor(svgWidth/60);

    var transform = (svgWidth - (2*sidePadding))/dataSize;
    var sentimentHeight = svgHeight - bottomPadding - topPadding;

    var yOrigin = svgHeight - bottomPadding;

    sentimentRange = range(0, dataSize - 1);
    sentimentRange = sentimentRange.map(point => point*transform + sidePadding);
    sentiment = sentiment.map(point => yOrigin - sentimentHeight*point);

    var zipSentiment = zip(sentimentRange, sentiment);

    sentimentLine = document.getElementById("sentimentLine");
    sentimentLine.setAttribute("points", zipSentiment);
    sentimentLine.setAttribute("stroke", "url(#gradient)");
    sentimentLine.setAttribute("stroke-width", "5");
    sentimentLine.setAttribute("stroke-linejoin", "round");
}

// --- Load JSON functions -----------------------------------------------------

function getJSON(jsonFile, callback) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', jsonFile, true);
    xhr.responseType = 'json';

    xhr.onload = function() {
        var status = xhr.status;
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    xhr.send();
}

function getJSONCallback(err, data) {
    if (err != null) { // log error
        console.error(err);
    }
    else {
        story = data;
        drawSentiment();
        moveMarker();
        loadText();
    }
}

function loadText() {
    var span = 8;
    var length = story.text.length;
    var low, up;

    if (markerIndex <= span) {
        low = 0;
        up = span;
    }
    else if (markerIndex >= length - span) {
        up = length;
        low = length - span;
    }
    else {
        low = markerIndex;
        up = markerIndex + span;
    }
    document.getElementById("text").innerHTML = story.text.slice(low, up).join(" ").trim();
    var title = document.getElementById("title").textContent = story.title;
    var author = document.getElementById("author").textContent = story.author;
}

// --- Utility Functions -------------------------------------------------------

function range(s, f) { // returns an array containing all numbers from s to f
    return (new Array(f - s + 1)).fill(undefined).map((_, i) => i + s);
}

function nearestIndex(num, arr) { // return nearest index to num in arr
    var mid;
    var low = 0;
    var high = arr.length - 1;
    while (high - low > 1) {
        mid = Math.floor((low + high)/2);
        if (arr[mid] < num) {
            low = mid;
        } else {
            high = mid;
        }
    }
    if (num - arr[low] <= arr[high] - num) {
        return low;
    }
    return high;
}

function rescale(array) { // min max normalization to range (0, 1)
    aMin = Math.min(...array);
    aMax = Math.max(...array);
    return array.map(a => (a - aMin)/(aMax - aMin));
}

// calculate gaussian function of given size
function gaussian(size, functionToWindow) {
    // functionToWindow relates σ of gaussian function to gaussian window

    if (size % 2 === 0) { // ensure the window size is odd
        size += 1;
    }

    sigma = size*functionToWindow; // σ = std of gaussian function

    // range of of windowSize centered at 0
    gaussianWindow = range(0-(size-1)/2, (size-1)/2);

    // return gaussian function in window: e^[(-1/2)(n/σ)^2]
    return gaussianWindow.map(n => Math.E**((-1/2)*((n/sigma)**2)));
}

// zip arr1 and arr2 into space/comma seperate string for svg path points
function zip(arr1, arr2) {
    var result = "";
    for (var i in arr1) {
        result = result.concat(arr1[i], ", ", arr2[i], " ");
    }
    return result;
}
