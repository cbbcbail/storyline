# --- Sentiment Analysis -------------------------------------------------------
# load books, cleanup text, analyze sentiment, plot, save sentiment scores
# by Connor Bailey, Fall 2020

### python libraries
import json     # used for loading and saving json data
import re       # used for text cleanup using regular expressions
import math

### 3rd party libraries
import matplotlib.pyplot as plt # for plotting
from scipy import signal # for generating gaussian functions
from sklearn.preprocessing import minmax_scale # for rescaling convolved data

# for sentiment analysis
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer

### local files
import escapes  # used for managing escapes for easy terminal output
import objects  # defines the story objects and related fucntions

def main(): # convert all text files to python objects analyze and save as json

    # titles of books
    titles = ["Frankenstein", "Pride and Prejudice",
              "The Count of Monte Cristo", "The Adventures of Tom Sawyer",
              "The Adventures of Huckleberry Finn"]

    # authors of books corresponding to titles
    authors = ["Mary Shelley", "Jane Austen", "Alexandre Dumas", "Mark Twain",
              "Mark Twain"]

    jsonFilePath = "../resources/json/" # path to json files
    txtFilePath = "../resources/txt/" # path to file
    escapes.hideCursor() # make cursor invisible in terminal

    for index, title in enumerate(titles):
        # print file analysis progress information with loading bar
        escapes.clearLine()
        print("Analyzing titles:", end = " ")
        escapes.changeColor(50, 147, 75, "background")
        print(" ".join(titles[:index]), end = "")
        escapes.changeColor(198, 149, 0, "background")
        print(" " + titles[index], end = " ")
        escapes.reset()
        print(" ".join(titles[index + 1:]))

        text, fileName = openText(title, txtFilePath) # load file
        sentences = cleanUp(text) # clean up text and break up by sentence
        sentiments = scoreSentiment(sentences) # score for each sentence
        plotStoryline(sentiments, title, index) # plot sentiments

        # create and save story data as an object in JSON
        story = objects.Story(title, authors[index], fileName, sentiments)
        saveAsJSON(story, fileName, jsonFilePath) # save story object as json
        escapes.moveCursor(1, "up") # move cursor up to update info

    # plt.show() # display generated plots

    # print conversion complete information before exiting program
    escapes.clearLine()
    print("Analysis completed successfully")
    escapes.clearLine()
    print("Stories saved at {}".format(jsonFilePath))
    escapes.showCursor() # unhide cursor in terminal before exiting program

def openText(title, txtFilePath):

    # convert title to file name to be opened and read from
    fileName = title.title().replace(" ", "")
    fileName = fileName[0].lower() + fileName[1:]

    # construct filepath, open file, and read full text
    txtFile = open(txtFilePath + fileName + ".txt", "r") # load input
    return txtFile.read(), fileName

# clean up text by removing unnecessary things and breaking it into sentences
def cleanUp(text):

    # remove periods which are unlikely to end sentences
    periodsRE = re.compile(r"(Mr|Mrs|Ms|Dr)\.")
    text = periodsRE.sub(periodSub, text)

    # remove minute markers
    minuteRE = re.compile(r"\d+m")
    text = minuteRE.sub("", text)

    # remove volume labels from the text
    volumeRE = re.compile(r"VOLUME \w+\n")
    text = volumeRE.sub("", text)

    # remove chapter labels from the text
    chapterRE = re.compile(r"(Chapter|CHAPTER) [A-Z0-9]+\.*\w*\n")
    text = chapterRE.sub("", text)

    # remove unnecessary symbols from the text
    symbolRE = re.compile(r"[“”‘’—]")
    text = symbolRE.sub("", text)

    # remove repeated periods
    whitespaceRE = re.compile(r"\.{2,}")
    text = whitespaceRE.sub(" ", text).strip()

    # replace large whitespace chunks with a single space
    whitespaceRE = re.compile(r"\s+")
    text = whitespaceRE.sub(" ", text).strip()

    # replace '_' emphasized words with all caps words to be recognized by VADER
    emphasisRE = re.compile(r"_[a-zA-Z ]+_")
    text = emphasisRE.sub(upperSub, text)

    # split the text into sentences and store as a list
    sentences = re.findall(r"(?! )[^.!?]*[.!?]", text)

    return sentences

def scoreSentiment(sentences):

    # calculate polarity scores using VADER sentiment analysis on each sentence
    sentiments = map(SentimentIntensityAnalyzer().polarity_scores, sentences)

    # extract only the compound score from each sentiment dictionary
    sentiments = [sentiment['compound'] for sentiment in list(sentiments)]

    # return a list of sentiments corresponding to each sentence in sentences
    return sentiments

def plotStoryline(sentiments, title, figureNumber=0):

    gaussianRatio = 2/3 # ratio of gaussian window size to overall dataset
    gaussianSize = math.floor(len(sentiments)*gaussianRatio)

    # stds are percentages of size of gaussian window to vary resolution
    percentages = [3, 6, 9, 15]

    plt.figure(figureNumber) # convolved data are plotted on specified figure
    gaussianAxis = plt.subplot(2, 1, 1)
    plt.title(title)
    dataAxis = plt.subplot(2, 1, 2)

    for percent in percentages:
        sigma = gaussianSize*percent/100 # σ = std of gaussian function

        # calculate gaussian for specified window size and standard deviation
        gaussian = signal.gaussian(gaussianSize, sigma)

        # convolve sentiments with specified gaussian while maintaining length
        convolveSentiment = signal.convolve(sentiments, gaussian, "same", "fft")

        # plot the gaussian functions and the corresponding convolved data
        gaussianAxis.plot(gaussian)
        dataAxis.plot(minmax_scale(convolveSentiment))

def saveAsJSON(story, fileName, jsonFilePath):
    jsonFile = open(jsonFilePath + fileName + ".json", 'w') # open json file
    json.dump(story, jsonFile, default=objects.objToDict) # save story
    jsonFile.close() # close the json file after dump

def upperSub(match): # removes '_' and uppercases matching substring
    return match.group()[1:-1].upper();

def periodSub(match): # removes '.' from end of matching substring
    return match.group()[:-1];

if __name__ == '__main__':
    main()
