(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
'use strict';

function FFT(size) {
  this.size = size | 0;
  if (this.size <= 1 || (this.size & (this.size - 1)) !== 0)
    throw new Error('FFT size must be a power of two and bigger than 1');

  this._csize = size << 1;

  // NOTE: Use of `var` is intentional for old V8 versions
  var table = new Array(this.size * 2);
  for (var i = 0; i < table.length; i += 2) {
    const angle = Math.PI * i / this.size;
    table[i] = Math.cos(angle);
    table[i + 1] = -Math.sin(angle);
  }
  this.table = table;

  // Find size's power of two
  var power = 0;
  for (var t = 1; this.size > t; t <<= 1)
    power++;

  // Calculate initial step's width:
  //   * If we are full radix-4 - it is 2x smaller to give inital len=8
  //   * Otherwise it is the same as `power` to give len=4
  this._width = power % 2 === 0 ? power - 1 : power;

  // Pre-compute bit-reversal patterns
  this._bitrev = new Array(1 << this._width);
  for (var j = 0; j < this._bitrev.length; j++) {
    this._bitrev[j] = 0;
    for (var shift = 0; shift < this._width; shift += 2) {
      var revShift = this._width - shift - 2;
      this._bitrev[j] |= ((j >>> shift) & 3) << revShift;
    }
  }

  this._out = null;
  this._data = null;
  this._inv = 0;
}
module.exports = FFT;

FFT.prototype.fromComplexArray = function fromComplexArray(complex, storage) {
  var res = storage || new Array(complex.length >>> 1);
  for (var i = 0; i < complex.length; i += 2)
    res[i >>> 1] = complex[i];
  return res;
};

FFT.prototype.createComplexArray = function createComplexArray() {
  const res = new Array(this._csize);
  for (var i = 0; i < res.length; i++)
    res[i] = 0;
  return res;
};

FFT.prototype.toComplexArray = function toComplexArray(input, storage) {
  var res = storage || this.createComplexArray();
  for (var i = 0; i < res.length; i += 2) {
    res[i] = input[i >>> 1];
    res[i + 1] = 0;
  }
  return res;
};

FFT.prototype.completeSpectrum = function completeSpectrum(spectrum) {
  var size = this._csize;
  var half = size >>> 1;
  for (var i = 2; i < half; i += 2) {
    spectrum[size - i] = spectrum[i];
    spectrum[size - i + 1] = -spectrum[i + 1];
  }
};

FFT.prototype.transform = function transform(out, data) {
  if (out === data)
    throw new Error('Input and output buffers must be different');

  this._out = out;
  this._data = data;
  this._inv = 0;
  this._transform4();
  this._out = null;
  this._data = null;
};

FFT.prototype.realTransform = function realTransform(out, data) {
  if (out === data)
    throw new Error('Input and output buffers must be different');

  this._out = out;
  this._data = data;
  this._inv = 0;
  this._realTransform4();
  this._out = null;
  this._data = null;
};

FFT.prototype.inverseTransform = function inverseTransform(out, data) {
  if (out === data)
    throw new Error('Input and output buffers must be different');

  this._out = out;
  this._data = data;
  this._inv = 1;
  this._transform4();
  for (var i = 0; i < out.length; i++)
    out[i] /= this.size;
  this._out = null;
  this._data = null;
};

// radix-4 implementation
//
// NOTE: Uses of `var` are intentional for older V8 version that do not
// support both `let compound assignments` and `const phi`
FFT.prototype._transform4 = function _transform4() {
  var out = this._out;
  var size = this._csize;

  // Initial step (permute and transform)
  var width = this._width;
  var step = 1 << width;
  var len = (size / step) << 1;

  var outOff;
  var t;
  var bitrev = this._bitrev;
  if (len === 4) {
    for (outOff = 0, t = 0; outOff < size; outOff += len, t++) {
      const off = bitrev[t];
      this._singleTransform2(outOff, off, step);
    }
  } else {
    // len === 8
    for (outOff = 0, t = 0; outOff < size; outOff += len, t++) {
      const off = bitrev[t];
      this._singleTransform4(outOff, off, step);
    }
  }

  // Loop through steps in decreasing order
  var inv = this._inv ? -1 : 1;
  var table = this.table;
  for (step >>= 2; step >= 2; step >>= 2) {
    len = (size / step) << 1;
    var quarterLen = len >>> 2;

    // Loop through offsets in the data
    for (outOff = 0; outOff < size; outOff += len) {
      // Full case
      var limit = outOff + quarterLen;
      for (var i = outOff, k = 0; i < limit; i += 2, k += step) {
        const A = i;
        const B = A + quarterLen;
        const C = B + quarterLen;
        const D = C + quarterLen;

        // Original values
        const Ar = out[A];
        const Ai = out[A + 1];
        const Br = out[B];
        const Bi = out[B + 1];
        const Cr = out[C];
        const Ci = out[C + 1];
        const Dr = out[D];
        const Di = out[D + 1];

        // Middle values
        const MAr = Ar;
        const MAi = Ai;

        const tableBr = table[k];
        const tableBi = inv * table[k + 1];
        const MBr = Br * tableBr - Bi * tableBi;
        const MBi = Br * tableBi + Bi * tableBr;

        const tableCr = table[2 * k];
        const tableCi = inv * table[2 * k + 1];
        const MCr = Cr * tableCr - Ci * tableCi;
        const MCi = Cr * tableCi + Ci * tableCr;

        const tableDr = table[3 * k];
        const tableDi = inv * table[3 * k + 1];
        const MDr = Dr * tableDr - Di * tableDi;
        const MDi = Dr * tableDi + Di * tableDr;

        // Pre-Final values
        const T0r = MAr + MCr;
        const T0i = MAi + MCi;
        const T1r = MAr - MCr;
        const T1i = MAi - MCi;
        const T2r = MBr + MDr;
        const T2i = MBi + MDi;
        const T3r = inv * (MBr - MDr);
        const T3i = inv * (MBi - MDi);

        // Final values
        const FAr = T0r + T2r;
        const FAi = T0i + T2i;

        const FCr = T0r - T2r;
        const FCi = T0i - T2i;

        const FBr = T1r + T3i;
        const FBi = T1i - T3r;

        const FDr = T1r - T3i;
        const FDi = T1i + T3r;

        out[A] = FAr;
        out[A + 1] = FAi;
        out[B] = FBr;
        out[B + 1] = FBi;
        out[C] = FCr;
        out[C + 1] = FCi;
        out[D] = FDr;
        out[D + 1] = FDi;
      }
    }
  }
};

// radix-2 implementation
//
// NOTE: Only called for len=4
FFT.prototype._singleTransform2 = function _singleTransform2(outOff, off,
                                                             step) {
  const out = this._out;
  const data = this._data;

  const evenR = data[off];
  const evenI = data[off + 1];
  const oddR = data[off + step];
  const oddI = data[off + step + 1];

  const leftR = evenR + oddR;
  const leftI = evenI + oddI;
  const rightR = evenR - oddR;
  const rightI = evenI - oddI;

  out[outOff] = leftR;
  out[outOff + 1] = leftI;
  out[outOff + 2] = rightR;
  out[outOff + 3] = rightI;
};

// radix-4
//
// NOTE: Only called for len=8
FFT.prototype._singleTransform4 = function _singleTransform4(outOff, off,
                                                             step) {
  const out = this._out;
  const data = this._data;
  const inv = this._inv ? -1 : 1;
  const step2 = step * 2;
  const step3 = step * 3;

  // Original values
  const Ar = data[off];
  const Ai = data[off + 1];
  const Br = data[off + step];
  const Bi = data[off + step + 1];
  const Cr = data[off + step2];
  const Ci = data[off + step2 + 1];
  const Dr = data[off + step3];
  const Di = data[off + step3 + 1];

  // Pre-Final values
  const T0r = Ar + Cr;
  const T0i = Ai + Ci;
  const T1r = Ar - Cr;
  const T1i = Ai - Ci;
  const T2r = Br + Dr;
  const T2i = Bi + Di;
  const T3r = inv * (Br - Dr);
  const T3i = inv * (Bi - Di);

  // Final values
  const FAr = T0r + T2r;
  const FAi = T0i + T2i;

  const FBr = T1r + T3i;
  const FBi = T1i - T3r;

  const FCr = T0r - T2r;
  const FCi = T0i - T2i;

  const FDr = T1r - T3i;
  const FDi = T1i + T3r;

  out[outOff] = FAr;
  out[outOff + 1] = FAi;
  out[outOff + 2] = FBr;
  out[outOff + 3] = FBi;
  out[outOff + 4] = FCr;
  out[outOff + 5] = FCi;
  out[outOff + 6] = FDr;
  out[outOff + 7] = FDi;
};

// Real input radix-4 implementation
FFT.prototype._realTransform4 = function _realTransform4() {
  var out = this._out;
  var size = this._csize;

  // Initial step (permute and transform)
  var width = this._width;
  var step = 1 << width;
  var len = (size / step) << 1;

  var outOff;
  var t;
  var bitrev = this._bitrev;
  if (len === 4) {
    for (outOff = 0, t = 0; outOff < size; outOff += len, t++) {
      const off = bitrev[t];
      this._singleRealTransform2(outOff, off >>> 1, step >>> 1);
    }
  } else {
    // len === 8
    for (outOff = 0, t = 0; outOff < size; outOff += len, t++) {
      const off = bitrev[t];
      this._singleRealTransform4(outOff, off >>> 1, step >>> 1);
    }
  }

  // Loop through steps in decreasing order
  var inv = this._inv ? -1 : 1;
  var table = this.table;
  for (step >>= 2; step >= 2; step >>= 2) {
    len = (size / step) << 1;
    var halfLen = len >>> 1;
    var quarterLen = halfLen >>> 1;
    var hquarterLen = quarterLen >>> 1;

    // Loop through offsets in the data
    for (outOff = 0; outOff < size; outOff += len) {
      for (var i = 0, k = 0; i <= hquarterLen; i += 2, k += step) {
        var A = outOff + i;
        var B = A + quarterLen;
        var C = B + quarterLen;
        var D = C + quarterLen;

        // Original values
        var Ar = out[A];
        var Ai = out[A + 1];
        var Br = out[B];
        var Bi = out[B + 1];
        var Cr = out[C];
        var Ci = out[C + 1];
        var Dr = out[D];
        var Di = out[D + 1];

        // Middle values
        var MAr = Ar;
        var MAi = Ai;

        var tableBr = table[k];
        var tableBi = inv * table[k + 1];
        var MBr = Br * tableBr - Bi * tableBi;
        var MBi = Br * tableBi + Bi * tableBr;

        var tableCr = table[2 * k];
        var tableCi = inv * table[2 * k + 1];
        var MCr = Cr * tableCr - Ci * tableCi;
        var MCi = Cr * tableCi + Ci * tableCr;

        var tableDr = table[3 * k];
        var tableDi = inv * table[3 * k + 1];
        var MDr = Dr * tableDr - Di * tableDi;
        var MDi = Dr * tableDi + Di * tableDr;

        // Pre-Final values
        var T0r = MAr + MCr;
        var T0i = MAi + MCi;
        var T1r = MAr - MCr;
        var T1i = MAi - MCi;
        var T2r = MBr + MDr;
        var T2i = MBi + MDi;
        var T3r = inv * (MBr - MDr);
        var T3i = inv * (MBi - MDi);

        // Final values
        var FAr = T0r + T2r;
        var FAi = T0i + T2i;

        var FBr = T1r + T3i;
        var FBi = T1i - T3r;

        out[A] = FAr;
        out[A + 1] = FAi;
        out[B] = FBr;
        out[B + 1] = FBi;

        // Output final middle point
        if (i === 0) {
          var FCr = T0r - T2r;
          var FCi = T0i - T2i;
          out[C] = FCr;
          out[C + 1] = FCi;
          continue;
        }

        // Do not overwrite ourselves
        if (i === hquarterLen)
          continue;

        // In the flipped case:
        // MAi = -MAi
        // MBr=-MBi, MBi=-MBr
        // MCr=-MCr
        // MDr=MDi, MDi=MDr
        var ST0r = T1r;
        var ST0i = -T1i;
        var ST1r = T0r;
        var ST1i = -T0i;
        var ST2r = -inv * T3i;
        var ST2i = -inv * T3r;
        var ST3r = -inv * T2i;
        var ST3i = -inv * T2r;

        var SFAr = ST0r + ST2r;
        var SFAi = ST0i + ST2i;

        var SFBr = ST1r + ST3i;
        var SFBi = ST1i - ST3r;

        var SA = outOff + quarterLen - i;
        var SB = outOff + halfLen - i;

        out[SA] = SFAr;
        out[SA + 1] = SFAi;
        out[SB] = SFBr;
        out[SB + 1] = SFBi;
      }
    }
  }
};

// radix-2 implementation
//
// NOTE: Only called for len=4
FFT.prototype._singleRealTransform2 = function _singleRealTransform2(outOff,
                                                                     off,
                                                                     step) {
  const out = this._out;
  const data = this._data;

  const evenR = data[off];
  const oddR = data[off + step];

  const leftR = evenR + oddR;
  const rightR = evenR - oddR;

  out[outOff] = leftR;
  out[outOff + 1] = 0;
  out[outOff + 2] = rightR;
  out[outOff + 3] = 0;
};

// radix-4
//
// NOTE: Only called for len=8
FFT.prototype._singleRealTransform4 = function _singleRealTransform4(outOff,
                                                                     off,
                                                                     step) {
  const out = this._out;
  const data = this._data;
  const inv = this._inv ? -1 : 1;
  const step2 = step * 2;
  const step3 = step * 3;

  // Original values
  const Ar = data[off];
  const Br = data[off + step];
  const Cr = data[off + step2];
  const Dr = data[off + step3];

  // Pre-Final values
  const T0r = Ar + Cr;
  const T1r = Ar - Cr;
  const T2r = Br + Dr;
  const T3r = inv * (Br - Dr);

  // Final values
  const FAr = T0r + T2r;

  const FBr = T1r;
  const FBi = -T3r;

  const FCr = T0r - T2r;

  const FDr = T1r;
  const FDi = T3r;

  out[outOff] = FAr;
  out[outOff + 1] = 0;
  out[outOff + 2] = FBr;
  out[outOff + 3] = FBi;
  out[outOff + 4] = FCr;
  out[outOff + 5] = 0;
  out[outOff + 6] = FDr;
  out[outOff + 7] = FDi;
};

},{}],2:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var FFT = _interopDefault(require('fft.js'));
var nextPOT = _interopDefault(require('next-power-of-two'));

function checkSize(size) {
  if (!Number.isInteger(size) || size < 1) {
    throw new TypeError(`size must be a positive integer. Got ${size}`);
  }
}

function checkKernel(kernel) {
  if (kernel.length === 0 || kernel.length % 2 !== 1) {
    throw new RangeError(
      `kernel must have an odd positive length. Got ${kernel.length}`
    );
  }
}

function checkBorderType(borderType) {
  if (borderType !== 'CONSTANT' && borderType !== 'CUT') {
    throw new RangeError(`unexpected border type: ${borderType}`);
  }
}

function checkInputLength(actual, expected) {
  if (actual !== expected) {
    throw new RangeError(
      `input length (${actual}) does not match setup size (${expected})`
    );
  }
}

function createArray(len) {
  const array = [];
  for (var i = 0; i < len; i++) {
    array.push(0);
  }
  return array;
}

class DirectConvolution {
  constructor(size, kernel, borderType = 'CONSTANT') {
    checkSize(size);
    checkKernel(kernel);
    checkBorderType(borderType);

    this.size = size;
    this.kernelOffset = (kernel.length - 1) / 2;
    this.outputSize =
      borderType === 'CONSTANT' ? size : size - 2 * this.kernelOffset;
    this.output = createArray(this.outputSize);
    this.kernel = kernel;
    this.kernelSize = kernel.length;
    this.borderType = borderType;
  }

  convolve(input) {
    checkInputLength(input.length, this.size);
    this.output.fill(0);
    if (this.borderType === 'CONSTANT') {
      this._convolutionBorder0(input);
    } else {
      this._convolutionBorderCut(input);
    }
    return this.output;
  }

  _convolutionBorder0(input) {
    for (let i = 0; i < this.size; i++) {
      for (let j = 0; j < this.kernelSize; j++) {
        this.output[i] +=
          interpolateInput(input, i - this.kernelOffset + j) * this.kernel[j];
      }
    }
  }

  _convolutionBorderCut(input) {
    for (let i = this.kernelOffset; i < this.size - this.kernelOffset; i++) {
      const index = i - this.kernelOffset;
      for (let j = 0; j < this.kernelSize; j++) {
        this.output[index] += input[index + j] * this.kernel[j];
      }
    }
  }
}

function directConvolution(input, kernel, borderType) {
  return new DirectConvolution(input.length, kernel, borderType).convolve(
    input
  );
}

function interpolateInput(input, idx) {
  if (idx < 0) return 0;
  else if (idx >= input.length) return 0;
  return input[idx];
}

class FFTConvolution {
  constructor(size, kernel, borderType = 'CONSTANT') {
    checkSize(size);
    checkKernel(kernel);
    checkBorderType(borderType);

    this.size = size;
    this.kernelOffset = (kernel.length - 1) / 2;
    this.doubleOffset = 2 * this.kernelOffset;
    this.borderType = borderType;
    const resultLength = size + this.doubleOffset;
    this.fftLength = nextPOT(Math.max(resultLength, 2));
    this.fftComplexLength = this.fftLength * 2;
    this.fft = new FFT(this.fftLength);

    kernel = kernel.slice().reverse();
    const paddedKernel = createArray(this.fftComplexLength);
    this.fftKernel = createArray(this.fftComplexLength);
    pad(kernel, paddedKernel, this.fftComplexLength);
    this.fft.transform(this.fftKernel, paddedKernel);

    this.paddedInput = createArray(this.fftComplexLength);
    this.fftInput = createArray(this.fftComplexLength);

    this.ifftOutput = createArray(this.fftComplexLength);
    this.result = paddedKernel;
  }

  convolve(input) {
    checkInputLength(input.length, this.size);
    pad(input, this.paddedInput, this.fftComplexLength);
    this.fft.transform(this.fftInput, this.paddedInput);

    for (var i = 0; i < this.fftInput.length; i += 2) {
      const tmp =
        this.fftInput[i] * this.fftKernel[i] -
        this.fftInput[i + 1] * this.fftKernel[i + 1];
      this.fftInput[i + 1] =
        this.fftInput[i] * this.fftKernel[i + 1] +
        this.fftInput[i + 1] * this.fftKernel[i];
      this.fftInput[i] = tmp;
    }

    this.fft.inverseTransform(this.ifftOutput, this.fftInput);
    const r = this.fft.fromComplexArray(this.ifftOutput, this.result);
    if (this.borderType === 'CONSTANT') {
      return r.slice(this.kernelOffset, this.kernelOffset + input.length);
    } else {
      return r.slice(this.doubleOffset, input.length);
    }
  }
}

function fftConvolution(input, kernel, borderType) {
  return new FFTConvolution(input.length, kernel, borderType).convolve(input);
}

function pad(data, out, len) {
  let i = 0;
  for (; i < data.length; i++) {
    out[i * 2] = data[i];
    out[i * 2 + 1] = 0;
  }

  i *= 2;
  for (; i < len; i += 2) {
    out[i] = 0;
    out[i + 1] = 0;
  }
}

const BorderType = {
  CONSTANT: 'CONSTANT',
  CUT: 'CUT'
};

exports.BorderType = BorderType;
exports.DirectConvolution = DirectConvolution;
exports.FFTConvolution = FFTConvolution;
exports.directConvolution = directConvolution;
exports.fftConvolution = fftConvolution;

},{"fft.js":1,"next-power-of-two":3}],3:[function(require,module,exports){
module.exports = nextPowerOfTwo

function nextPowerOfTwo (n) {
  if (n === 0) return 1
  n--
  n |= n >> 1
  n |= n >> 2
  n |= n >> 4
  n |= n >> 8
  n |= n >> 16
  return n+1
}
},{}],4:[function(require,module,exports){
// imports
const convolution = require('ml-convolution'); // import the convolution package

// global vars
var story;
var functionToWindow;
var svgWidth;
var svgHeight;
var gaussianHeight;
var sidePadding;
var sentimentRange;
var sentiment;
var markerIndex = 0;
var markerX;

// html elements
const marker = document.getElementById("marker");
const svg = document.getElementById("svg");
const gaussianSlider = document.getElementById("gaussianSlider");
const titles = ["aChristmasCarol", "alicesAdventuresInWonderland",
                "aMidsummerNightsDream", "aTaleOfTwoCities", "dracula",
                "frankenstein", "greatExpectations", "hamlet",
                "heartOfDarkness", "littleWomen", "macbeth", "moby-Dick",
                "prideAndPrejudice", "romeoAndJuliet", "theCountOfMonteCristo",
                "theIliad", "theOdyssey", "thePictureOfDorianGray",
                "theScarletLetter", "theSecretGarden", "treasureIsland",
                "uncleTomsCabin", "warAndPeace"];
var index;

// initialization when page is loaded
window.onload = loadPage();

// --- Event Functions ---------------------------------------------------------

function loadPage() {
    markerX = 0;
    const params = new URLSearchParams(window.location.search);
    index = params.get("index");
    if (index == null) {
        index = Math.floor(Math.random()*titles.length)
    }

    // initialize onclick functions for cardLeft and cardRight
    document.getElementById("cardLeft").onclick = function() {
        if (index == 0) {
            index = titles.length - 1;
        }
        else {
            index--;
        }
        loadStory(index);
    };
    document.getElementById("cardRight").onclick = function() {
        if (index == titles.length - 1) {
            index = 0;
        }
        else {
            index++;
        }
        loadStory(index);
    };

    // initialize gaussian slider
    gaussianSlider.min = 2;
    gaussianSlider.max = 16;
    gaussianSlider.value = 7;
    gaussianSlider.step = 0.1;
    gaussianSlider.onchange = loadSlider;
    gaussianSlider.oninput = loadSlider;

    // add event listeners for resizing ot reorienting window
    window.addEventListener('resize', loadPlot, false);
    window.addEventListener('orientationchange', loadPlot, false);
    svg.addEventListener('mousemove', moveMarker);
    loadStory(index);
    loadPlot();
}

function loadStory(index) {
    // load title from the set
    markerX = 0;
    gaussianSlider.value = 7;
    functionToWindow = gaussianSlider.value/100;
    getJSON(titles[index] + ".json", getJSONCallback);
}

function loadSlider() { // event function for slider change/input
    functionToWindow = gaussianSlider.value/100;
    drawGaussian();
    if (story) {
        drawSentiment();
        moveMarker();
    }
}

function loadPlot() { // event function for window resize/reorientation
    svgSize();
    drawGaussian();
    if (story) {
        drawSentiment();
        moveMarker();
    }
}

function moveMarker() {
    var min = sidePadding;
    var max = svgWidth - sidePadding;
    var pt = svg.createSVGPoint();

    if (event !== undefined && event.clientX !== undefined) {
        pt.x = event.clientX;
        var svgP = pt.matrixTransform(svg.getScreenCTM().inverse());
        markerX = svgP.x;
    }

    if (markerX < min) {
        markerX = min;
    }
    else if (markerX > max) {
        markerX = max;
    }

    markerIndex = nearestIndex(markerX, sentimentRange);
    marker.setAttribute("cx", markerX);
    marker.setAttribute("cy", sentiment[markerIndex]);
    marker.setAttribute("visibility", "visible");
    loadText();
}

function svgSize() { // sets canvas size
    const widthRatio = 0.75; // ratio of canvas width to window width
    const heightRatio = 0.2; // ratio of canvas width to canvas height

    svgWidth = window.innerWidth*widthRatio;
    svgHeight = Math.min(window.innerHeight*heightRatio, svgWidth*heightRatio);
    svg.setAttribute("viewBox", "0 0 " + svgWidth + " " + svgHeight);
}

// --- Drawing Functions -------------------------------------------------------

function drawGaussian() { // draws gaussian function in corner of canvas
    gaussianHeight = svgHeight/15;
    var gaussianWidth = Math.floor(svgWidth/20);
    var bottomPadding = svgHeight/25;
    var rightSidePadding = Math.floor(svgWidth/60);

    var xOrigin = svgWidth - gaussianWidth - rightSidePadding;
    var yOrigin = svgHeight - bottomPadding;

    var gaussianRange = range(xOrigin, xOrigin + gaussianWidth - 1);
    var gaussianArray = gaussian(gaussianWidth, functionToWindow);
    gaussianArray = gaussianArray.map(point => yOrigin - gaussianHeight*point);

    var zipGaussian = zip(gaussianRange, gaussianArray);

    gaussianLine = document.getElementById("gaussianLine");
    gaussianLine.setAttribute("points", zipGaussian);
    gaussianLine.setAttribute("stroke-linejoin", "round");
}

function drawSentiment() { // draws the canvas paths on the canvas context
    var dataSize = story.sentiments.length;
    var windowToOverall = 2/3; // relate gaussian window size to overall dataset
    var windowSize = Math.floor(dataSize*windowToOverall);

    var gaussianArray = gaussian(windowSize, functionToWindow);
    sentiment = convolution.fftConvolution(story.sentiments, gaussianArray);
    sentiment = rescale(sentiment);

    var bottomPadding = svgHeight/5;
    var topPadding = svgHeight/25;
    sidePadding = Math.floor(svgWidth/60);

    var transform = (svgWidth - (2*sidePadding))/dataSize;
    var sentimentHeight = svgHeight - bottomPadding - topPadding;

    var yOrigin = svgHeight - bottomPadding;

    sentimentRange = range(0, dataSize - 1);
    sentimentRange = sentimentRange.map(point => point*transform + sidePadding);
    sentiment = sentiment.map(point => yOrigin - sentimentHeight*point);

    var zipSentiment = zip(sentimentRange, sentiment);

    sentimentLine = document.getElementById("sentimentLine");
    sentimentLine.setAttribute("points", zipSentiment);
    sentimentLine.setAttribute("stroke", "url(#gradient)");
    sentimentLine.setAttribute("stroke-width", "5");
    sentimentLine.setAttribute("stroke-linejoin", "round");
}

// --- Load JSON functions -----------------------------------------------------

function getJSON(jsonFile, callback) {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', jsonFile, true);
    xhr.responseType = 'json';

    xhr.onload = function() {
        var status = xhr.status;
        if (status == 200) {
            callback(null, xhr.response);
        } else {
            callback(status);
        }
    };
    xhr.send();
}

function getJSONCallback(err, data) {
    if (err != null) { // log error
        console.error(err);
    }
    else {
        story = data;
        drawSentiment();
        moveMarker();
        loadText();
    }
}

function loadText() {
    var span = 8;
    var length = story.text.length;
    var low, up;

    if (markerIndex <= span) {
        low = 0;
        up = span;
    }
    else if (markerIndex >= length - span) {
        up = length;
        low = length - span;
    }
    else {
        low = markerIndex;
        up = markerIndex + span;
    }
    document.getElementById("text").innerHTML = story.text.slice(low, up).join(" ").trim();
    var title = document.getElementById("title").textContent = story.title;
    var author = document.getElementById("author").textContent = story.author;
}

// --- Utility Functions -------------------------------------------------------

function range(s, f) { // returns an array containing all numbers from s to f
    return (new Array(f - s + 1)).fill(undefined).map((_, i) => i + s);
}

function nearestIndex(num, arr) { // return nearest index to num in arr
    var mid;
    var low = 0;
    var high = arr.length - 1;
    while (high - low > 1) {
        mid = Math.floor((low + high)/2);
        if (arr[mid] < num) {
            low = mid;
        } else {
            high = mid;
        }
    }
    if (num - arr[low] <= arr[high] - num) {
        return low;
    }
    return high;
}

function rescale(array) { // min max normalization to range (0, 1)
    aMin = Math.min(...array);
    aMax = Math.max(...array);
    return array.map(a => (a - aMin)/(aMax - aMin));
}

// calculate gaussian function of given size
function gaussian(size, functionToWindow) {
    // functionToWindow relates σ of gaussian function to gaussian window

    if (size % 2 === 0) { // ensure the window size is odd
        size += 1;
    }

    sigma = size*functionToWindow; // σ = std of gaussian function

    // range of of windowSize centered at 0
    gaussianWindow = range(0-(size-1)/2, (size-1)/2);

    // return gaussian function in window: e^[(-1/2)(n/σ)^2]
    return gaussianWindow.map(n => Math.E**((-1/2)*((n/sigma)**2)));
}

// zip arr1 and arr2 into space/comma seperate string for svg path points
function zip(arr1, arr2) {
    var result = "";
    for (var i in arr1) {
        result = result.concat(arr1[i], ", ", arr2[i], " ");
    }
    return result;
}

},{"ml-convolution":2}]},{},[4]);
