# --- Escapes ------------------------------------------------------------------
# handles escape codes for easily modifying color & cursor in terminal output
# by Connor Bailey

# 033_oct = 27_dec = x1b_hex = character e, but 033 seems to be most used

# cursor becomes invisible until showCursor is run
def hideCursor():
    print("\033[?25l", end = "") # print hide cursor escape string

# cursor becomes visible again until hideCursor is run
def showCursor():
    print("\033[?25h", end = "") # print show cursor escape string

# color or style is reset to original values from before any escape codes ran
def reset():
	print("\033[0m", end = "") # print reset color escape string

# clear all characters from current line
def clearLine():
    print("\033[0K", end = "") # print clear line escape string

# change print style to bold
def makeBold():
    print("\033[1m", end = "") # print clear line escape string

# move the cursor n steps in direction specified
def moveCursor(n, direction):
    errorString = "Error: moveCursor direction unrecognized" # in case of error
    switcher = { # dictionary mapping direction to ISO escape string character
        "up": "\033[" + str(n) + "A",
        "down": "\033[" + str(n) + "B",
        "right": "\033[" + str(n) + "C",
        "left": "\033[" + str(n) + "D"
    }
    print(switcher.get(direction, errorString), end = "") # print escape string

# change print color to the given RGB values for the background or foreground
def changeColor(r, g, b, ground):
    # ensure rgb values are formatted as strings for printing
    r = str(r)
    g = str(g)
    b = str(b)

    if ground == "background": # print ISO escape string to change background
        print("\033[48:2:2:" + r + ":" + g + ":" + b + "m", end = "")
    elif ground == "foreground": # print ISO escape string to change foreground
        print("\033[38:2:2:" + r + ":" + g + ":" + b + "m", end = "")
    else: # otherwise print in case of error
        print("Error: changeColor ground unrecognized")

class escapes(object):
    def __format__(self, format):
        if (format == 'makeBold'):
            return "\033[1m"
        if (format == 'reset'):
            return "\033[0m"
